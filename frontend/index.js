let api = 'http://localhost:3000/',
    btnLinks = document.querySelectorAll('.btn-link-shit'),
    place = document.querySelectorAll('.place'),
    arrays = {
        news: [],
        products: [],
        users: [],
    }



axios.get(api + "news")
    .then(res => {
        render(res.data["news"], "news")
        arrays["news"] = res.data["news"]
    })
    .catch(err => {
        console.log(err);
    })

btnLinks.forEach((item, index) => {
    item.onclick = (event) => {
        let href = item.getAttribute('href').split('/')[0]
        event.preventDefault()
        
        if (arrays[href].length > 1) {
            render(arrays[href], href)
        
        } else {
            axios.get(api + href)
                .then(res => {
                    render(res.data[href], href)
                    arrays[href] = res.data[href]
                })
                .catch(err => {
                    console.log(err);
                })
        }
    }
})

const render = (arr, what) => {
    place.forEach(item => {
        item.innerHTML = ''
    })
    for (let item of arr) {
        if (what == 'products') {
            document.querySelector('.' + what).innerHTML += `
            <div class="item">
            <h2>${item.name}</h2>
            <p><b>${item.price}$</b></p>
            <p>${item.title}</p>    
        </div>
            `
        }
        if (what == 'users') {
            document.querySelector('.' + what).innerHTML += `
            <div class="item">
            <h2>Name: ${item.name}</h2>
            <p>Phone:${item.phone} </p>
            <p>Photos: ${item.photos}</p>
            <p>Vidios: ${item.vidios}</p>
            <p>Voices: ${item.voices}</p>
            <h4>Links: ${item.links}</h4>
        </div>
            `
        }
        if (what == 'news') {
            document.querySelector('.' + what).innerHTML += `
            
            <a href='${"./product-news-users.html#" + item.id}'>
                <div class="item">
                    <h2>${item.title}</h2>
                    <p>${item.text}</p>
                    <p>${item.id}</p>
                </div>
            </a>
            
            `
        }
    }
}