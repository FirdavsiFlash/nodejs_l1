// Импортируем наш фреймворк
const express = require("express")
// Создаем главную переменную
const app = express()
const bodyParse = require("body-parser")
const cors = require('cors')
const mongoose = require('mongoose')


// Middlewares
app.use(cors())
app.use(bodyParse.json())

// Now
app.use("/news", require("./routes/new.js"))
app.use("/users", require("./routes/users.js"))
app.use("/products", require("./routes/products.js"))

// Data Base
mongoose.connect("monhodb://localhost:27017", () => {
    console.log('Data Base is working!');
})

 
 mongoose.set("debug", true)

// Слушатель по порту
app.listen(3000, () => {
    console.log("Сервер слушает порт 3000")
})