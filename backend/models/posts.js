const mongoose = require("mongoose")


var Posts = mongoose.Schema({
    title: {
        type: String,
        trim: true,
        required: true
    },
    text: {
        type: String,
        trim: true,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    comments: [{
        text: {
            type: String,
            trim: true,
            required: true
        },
        date: {
            type: Date,
            default: Date.now
        }
    }]
})

module.exports = mongoose.model('Posts', Posts)