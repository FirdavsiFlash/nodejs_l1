const mongoose = require("mongoose")

var Users = mongoose.Schema({
    name: {
        first: String,
        last: String,
        lowercase: true
    },
    phone: {
        present: {
            type: Number,
            min: 12,
            max: 20
        },
        reserve: {
            type: Number,
            min: 12,
            max: 20
        }
    },
    age: {
        type: Number,
        min: 18,
        max: 65
    },
    city: {
        type: String,
        lowercase: true
    },
    email: String,
    dateBorn: Date,
    accounts: {
        fbAccount: {
            foo: String,
            bar: String,
            baz: String,
            link: String,
            created: {
                type: Date,
                default: Date.now
            },
        },
        googleAccount: {
            foo: String,
            bar: String,
            link: String,
            created: {
                type: Date,
                default: Date.now
            },
        },
        instAccount: {
            foo: String,
            bar: String,
            link: String,
            created: {
                type: Date,
                default: Date.now
            },
        }
    },
    additionalData: {
        type: subSchema,
        default: {},
        mother: {
            type: subSchema,
            default: {},
            name: {
                first: String,
                last: String,
                lowercase: true
            },
            phone: {
                present: {
                    type: Number,
                    min: 12,
                    max: 20
                },
                reserve: {
                    type: Number,
                    min: 12,
                    max: 20
                }
            },
            age: {
                type: Number,
                min: 18,
                max: 65
            },
            city: {
                type: String,
                lowercase: true
            },
            email: String,
            dateBorn: Date,
        },
        father: {
            type: subSchema,
            default: {},
            name: {
                first: String,
                last: String,
                lowercase: true
            },
            phone: {
                present: {
                    type: Number,
                    min: 12,
                    max: 20
                },
                reserve: {
                    type: Number,
                    min: 12,
                    max: 20
                }
            },
            age: {
                type: Number,
                min: 18,
                max: 65
            },
            city: {
                type: String,
                lowercase: true
            },
            email: String,
            dateBorn: Date,
        },
        default: {}
    },
});

module.exports = mongoose.model('Users', Users)