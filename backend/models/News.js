const mongoose = require("mongoose")
const root = 'https://s3.amazonaws.com/mybucket';




var News = mongoose.Schema({
    authors: {
        type: String,
        required: true
    },
    head: {
        type: String,
        required: true
    },
    titles: {
        type: [String],
        required: true
    },
    texts: {
        type: [String],
        required: true
    },
    links: {
        type: [String],
        required: false
    },
    date: {
        type: Date,
        required: true
    },
    socialMedias: {
        instagram: 'vkarpov15',
        twitter: '@code_barbarian',
        required: false
    },
    picture: {
        type: String,
        get: v => `${root}${v}`,
        required: false
    },
    likes: {
        type: Number,
        default: 0
    },
    comments: [{
        user: String,
        text: {
            type: String,
            trim: true,
            required: true
        },
        date: {
            type: Date,
            default: Date.now,
            required: true
        }
    }]
})

const mongoose = require('mongoose');

module.exports = mongoose.model('News', News)